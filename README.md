# 基于syszuxpinyin的Qt嵌入式系统汉字输入法

#### 介绍
syszuxpinyin是一个开源的嵌入式系统qt输入法，这个输入法原来是GBK的编码格式，这里我修改为UT8格式，并修改了一些别的地方；在这里还要感谢syszuxpinyin的开源者！
可以满足嵌入式系统常用输入需求，数字、字母、符号、常用汉字等


#### 使用说明
将动态库放到qt的默认库文件目录下即可，
在qt的程序中添加以下代码：
包含头文件：

#ifdef DEVICE

#include <QWSServer>

#include "syszuxim.h"

#ifdef DEVICE

    QWSServer::setCursorVisible(false);
	
    QWSInputMethod* im = new SyszuxIM;
    
	QObject::connect(pEventFilter, SIGNAL(noOperationDetect()), im, SLOT(OnNoOpreationTimeOut()));
    
	QWSServer::setCurrentInputMethod(im);

#endif

由于在PC上编译时，不支持QWSServer，所以我这里加了编译开关；

在输入法代码里，已经连接了获取焦点的信号，所以当界面上的输入框获取到焦点时，

输入法会自动显示出来；


微信订阅号：fensTeck