# 基于syszuxpinyin的Qt嵌入式系统汉字输入法

#### Description
syszuxpinyin是一个开源的嵌入式系统qt输入法，这个输入法原来是GBK的编码格式，这里我修改为UT8格式，并修改了一些别的地方；在这里还要感谢syszuxpinyin的开源者！
可以满足嵌入式系统常用输入需求，数字、字母、符号、常用汉字等


#### Software Architecture
Software architecture description

#### Installation

1. xxxx
2. xxxx
3. xxxx

#### Instructions

1. xxxx
2. xxxx
3. xxxx

#### Contribution

1. Fork the repository
2. Create Feat_xxx branch
3. Commit your code
4. Create Pull Request


#### Gitee Feature

1. You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2. Gitee blog [blog.gitee.com](https://blog.gitee.com)
3. Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4. The most valuable open source project [GVP](https://gitee.com/gvp)
5. The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6. The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)